package hu.gergo.kovacs.practicegermanwords.model

/**
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.07.17.
 */

data class Word(val english: String, val german: String)



