package hu.gergo.kovacs.practicegermanwords.parser.csv

import hu.gergo.kovacs.practicegermanwords.config.ApplicationHelper
import java.io.IOException
import java.io.InputStream

/**
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.07.17.
 */
class CSVReader<ObjectType> {
    /**
     * Acquires and opens the file as an InputStream then passes it to parseStreamIntoObjects() for
     * parsing
     *
     * @param idOfCSVFilePathString the id of the string resource of the CSV file location (eg R.string.my_resource)
     * @return the list of objects
     */
    fun loadAndParseFile(idOfCSVFilePathString: Int, objectConstructor: (List<String>) -> (ObjectType)): List<ObjectType> {
        // acquiring the filepath as a string
        val context = ApplicationHelper.context
        val filePath = context.getString(idOfCSVFilePathString)

        // opening and parsing the file
        var entries: List<ObjectType> = emptyList()
        try {
            context.getAssets().open(filePath).use { csvInputStream ->
                entries = parseStreamIntoObjects(csvInputStream, objectConstructor)
            }
        } catch (exception: IOException) {
            exception.printStackTrace()
        }
        return entries
    }

    /**
     * Parses the inputStream into objects than returns them as a list.
     *
     * @param inputStream the input stream of the file to be parsed
     * @param objectConstructor the constructor lambda that create an ObjectType object form a parsed line
     * @return the list of objects
     */
    @Throws(IOException::class)
    private fun parseStreamIntoObjects(inputStream: InputStream,
                                       objectConstructor: (List<String>) -> (ObjectType)): List<ObjectType> {
        val separatorCharacter = ":"
        var parsedLine: List<String>
        var parsedObject: ObjectType
        val objectList: MutableList<ObjectType> = arrayListOf()

        inputStream.bufferedReader().useLines {
            it.toList().forEach { nextLine ->
                parsedLine = nextLine.split(separatorCharacter)
                parsedObject = objectConstructor(parsedLine)
                objectList.add(parsedObject)
            }
        }

        return objectList
    }
}


