package hu.gergo.kovacs.practicegermanwords.ui

import android.content.Intent
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import hu.gergo.kovacs.practicegermanwords.R
import hu.gergo.kovacs.practicegermanwords.view.adapter.MainMenuAdapter

/**
 * The main menu activity of the application. It is the launcher activity and it serves as a central
 * starting point for the application
 *
 * @author Kovacs Gergo
 *
 * @version 1.0
 *
 * @date 2019.07.17.
 */
class MainMenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_menu_activity)
        setupMainMenu()
    }

    private fun setupMainMenu() {
        val listView: ListView = this.findViewById(R.id.main_listview)
        val title = resources.getStringArray(R.array.main_menu_list)
        val descriptions = resources.getStringArray(R.array.main_menu_list_description)

        listView.adapter =  MainMenuAdapter(this, title, descriptions)

        listView.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            when (position) {
                0 -> {
                        val intent = Intent(this, AllWordsActivity::class.java)
                        startActivity(intent)
                }
            }
        }
    }
}
