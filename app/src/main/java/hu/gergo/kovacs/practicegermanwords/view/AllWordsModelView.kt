package hu.gergo.kovacs.practicegermanwords.view

import android.provider.UserDictionary
import androidx.lifecycle.ViewModel
import hu.gergo.kovacs.practicegermanwords.R
import hu.gergo.kovacs.practicegermanwords.model.Word
import hu.gergo.kovacs.practicegermanwords.parser.csv.CSVReader

/**
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.07.17.
 */

class AllWordsModelView : ViewModel() {
    private val words: List<Word> = loadWords()
    private var currentWord = 0

    private fun loadWords(): List<Word> {
        return CSVReader<Word>().loadAndParseFile(R.string.german_words_02) {
            if (it.size != 2)
                throw IndexOutOfBoundsException("the size of array was ${it.size} but expected 2")
            Word(it[0], it[1])
        }
    }

    public fun getNextWord(): Word {
        if(++currentWord >= words.size)
            currentWord = 0
        return words[currentWord]
    }

    public fun getRandomWord(): Word {
        return words.random()
    }
}



