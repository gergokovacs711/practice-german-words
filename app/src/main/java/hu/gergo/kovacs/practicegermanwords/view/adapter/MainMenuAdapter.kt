package hu.gergo.kovacs.practicegermanwords.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import hu.gergo.kovacs.practicegermanwords.R
import hu.gergo.kovacs.practicegermanwords.config.ApplicationHelper

/**
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.07.17.
 */
class MainMenuAdapter(
    context: Context,
    private val titleArray: Array<String>,
    private val descriptionArray: Array<String>)
    : BaseAdapter() {
    private val layoutInflater = LayoutInflater.from(context)
    private var title: TextView? = null
    private var description: TextView? = null
    private var imageView: ImageView? = null

    override fun getCount(): Int {
        return titleArray.size
    }

    override fun getItem(position: Int): Any {
        return titleArray[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val mainMenuItemView = view ?: layoutInflater.inflate(R.layout.main_menu_item, null)

        title = mainMenuItemView.findViewById(R.id.main_item_title) as TextView
        description = mainMenuItemView.findViewById(R.id.main_item_description) as TextView
        imageView = mainMenuItemView.findViewById(R.id.main_item_image) as ImageView

        title?.text = titleArray[position]
        description?.text = descriptionArray[position]

        // setting menu icon from local image array
        val imageArray = ApplicationHelper.context.resources.obtainTypedArray(R.array.main_menu_icon_images)
        imageView?.setImageResource(imageArray.getResourceId(position, -1))
        imageArray.recycle()

        return mainMenuItemView
    }
}


