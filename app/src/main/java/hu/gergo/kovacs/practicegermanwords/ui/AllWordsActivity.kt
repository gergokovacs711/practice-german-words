package hu.gergo.kovacs.practicegermanwords.ui

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.ToggleButton
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProviders
import hu.gergo.kovacs.practicegermanwords.R
import hu.gergo.kovacs.practicegermanwords.model.Word
import hu.gergo.kovacs.practicegermanwords.view.AllWordsModelView

/**
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.07.17.
 */

class AllWordsActivity : AppCompatActivity() {

    lateinit var allWordsModelView: AllWordsModelView
    lateinit var germanText: TextView
    lateinit var englishText: TextView
    lateinit var button: Button
    lateinit var currentWord: Word
    lateinit var randomButton: ToggleButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.all_words_activity)
        germanText = findViewById(R.id.text_view_german)
        englishText = findViewById(R.id.text_view_english)
        button = findViewById(R.id.action_button)
        button.setOnClickListener { nextButton(it) }
        randomButton = findViewById(R.id.randomButton)
        randomButton.toggle()

        allWordsModelView = ViewModelProviders.of(this).get(AllWordsModelView::class.java)
        currentWord = allWordsModelView.getNextWord()

        englishText.text = currentWord.english
        germanText.text = currentWord.german
        germanText.visibility = View.INVISIBLE
    }

    public fun nextButton(view: View){
        view as Button
        if(germanText.isVisible) {
            germanText.visibility = View.INVISIBLE
            currentWord = if(!randomButton.isChecked) allWordsModelView.getNextWord() else allWordsModelView.getRandomWord()
            germanText.text = currentWord.german
            englishText.text = currentWord.english
            view.text = "Show answer"
        } else {
            germanText.visibility = View.VISIBLE
            view.text = "Next word"
        }
    }
}

