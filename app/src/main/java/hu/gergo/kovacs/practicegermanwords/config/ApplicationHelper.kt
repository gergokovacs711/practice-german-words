package hu.gergo.kovacs.practicegermanwords.config

import android.app.Application
import android.content.Context

/**
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.07.17.
 */
class ApplicationHelper : Application() {

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }

    companion object {
        private var INSTANCE: ApplicationHelper? = null

        /**
         * Get context
         *
         * @return the context
         */
        val context: Context
            get() = INSTANCE!!.applicationContext
    }
}


